package com.app.dsgs;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(
		info = @Info(
				title = "Maharishi Microservices project -  SA CS590 final Feb 2024",
				description = "Microservices APIs using Kafka Pipeline",
				version = "v1.0.0",
				contact = @Contact(
						name = "Team Planet",
						email = "dunkygeoffrey39@gmail.com",
						url = "https://www.miu.edu/"
				),
				license = @License(
						name = "Apache 2.0",
						url = "https://www.miu.edu/"
				)
		),
		externalDocs = @ExternalDocumentation(
				description = "Software Architecture final project development",
				url = ""
		)
)

@SpringBootApplication
public class DsgsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DsgsApplication.class, args);
	}

}
