package com.app.dsgs.utility;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Constants {

    public static final String DOCKER_NETWORK = "testing";

    public static final String PROJECT_ROOT = "C:\\Users\\G\\Desktop\\Software Architecture\\dynamicservicegeneratorservice\\RTDS";

    public static class DockerPaths {
        public static final String RTDIS = PROJECT_ROOT + "\\Dockerfile";
    }

    public static class APP_PORTS {
        public static final String RTDS = "8081";
    }

    public static Map<String, String> PORTS;
    static {
        PORTS = new HashMap<>();
        PORTS.put("RTDS", APP_PORTS.RTDS);
        PORTS = Collections.unmodifiableMap(PORTS);
    }
}
