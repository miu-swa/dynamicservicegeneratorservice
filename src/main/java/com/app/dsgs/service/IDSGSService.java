package com.app.dsgs.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.util.List;

public interface IDSGSService {
    void createRTDIS();
    void createJDRS(ConsumerRecord<?,?> message);
    void createCDS(ConsumerRecord<?,?> message);
    void createSS(ConsumerRecord<?,?> message);
    boolean getJDRS();
    void setJDRS(boolean jdrs);
    List<Integer> getCDlist();
    void setCD(int id);
    int getCDId();
    void setCDId(int id);
}
