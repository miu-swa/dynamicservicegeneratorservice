package com.app.dsgs.service.impl;

import com.app.dsgs.service.IDSGSService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DSGSServiceImpl implements IDSGSService {
    public boolean jdrs;
    public List<Integer> cdArray;
    public int lastCd;
    public DSGSServiceImpl(){
        cdArray = new ArrayList<>();
        jdrs = false;
        lastCd = 0;
    }

    @Override
    public void createRTDIS() {
        System.out.println("JDRS number is instantiated.");
    }

    @Override
    public void createJDRS(ConsumerRecord<?,?> message) {
        System.out.println("JDRS number is instantiated.");
        System.out.println("value:"+message.value());
    }

    @Override
    public void createCDS(ConsumerRecord<?,?> message) {
        System.out.println("CDS number is instantiated.");
        System.out.println("value:"+message.value());
    }

    @Override
    public void createSS(ConsumerRecord<?,?> message) {
        //instantiated SS services
        //check cdArray
        System.out.println("value:"+message.value());
        if(lastCd > 1) {
            int n = cdArray.size() - 1;
            for (int i = 0; i < n; i++) {
                //it's supposed to instantiate SS services
                System.out.println("SS number "+i +" is instantiated.");
            }
        }
    }

    @Override
    public boolean getJDRS() {
        return jdrs;
    }

    @Override
    public void setJDRS(boolean jdrs) {
        this.jdrs = jdrs;
    }

    @Override
    public List<Integer> getCDlist() {
        return cdArray;
    }

    @Override
    public void setCD(int id) {
        cdArray.add(id);
    }

    @Override
    public int getCDId() {
        return lastCd;
    }

    @Override
    public void setCDId(int id) {
        lastCd = id;
    }
}
