package com.app.dsgs.service.impl;

import com.app.dsgs.utility.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class RunContainer {

    private static final Logger LOG = LoggerFactory.getLogger(RunContainer.class);
    private static int dynamicPort = 8081;
    private RunContainer() {}

    public static void run(String commandLineArg, String instance) {
        String projectRoot = Constants.PROJECT_ROOT;
        String dockerfilePath = Constants.DockerPaths.RTDIS;
        double rnd = Math.random();
        String imageName = "image-" + rnd;
        String containerName = "container-" + rnd;

        buildDockerImage(projectRoot, dockerfilePath, imageName);

        runDockerContainer(imageName, containerName, commandLineArg, instance);
    }

    private static void buildDockerImage(String projectRoot, String dockerfilePath, String imageName) {
        try {
            ProcessBuilder processBuilder = new ProcessBuilder(
                    "docker", "build",
                    "-t", imageName,
                    "-f", dockerfilePath,
                    projectRoot
            );

            processBuilder.redirectErrorStream(true);

            Process process = processBuilder.start();
            process.waitFor();

//            printProcessOutput(process);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void runDockerContainer(String imageName, String containerName,
                                        String url, String instance) {

        try {
            String port = ++dynamicPort + ":" + Constants.PORTS.get(instance.toUpperCase());

            ProcessBuilder processBuilder = new ProcessBuilder(
                    "docker", "run",
                    "--name", containerName,
                    "-p", port,
                    "--network", Constants.DOCKER_NETWORK,
                    imageName,
                    url
            );

            processBuilder.redirectErrorStream(true);
            Process process = processBuilder.start();
            process.waitFor();

//                printProcessOutput(process);
        } catch (IOException | InterruptedException e) {
            LOG.error(e.getMessage(), e);
        }

    }

    private static void printProcessOutput(Process process) throws IOException {
        java.util.Scanner s = new java.util.Scanner(process.getInputStream()).useDelimiter("\\A");
        String output = s.hasNext() ? s.next() : "";
        System.out.println(output);
    }
}

