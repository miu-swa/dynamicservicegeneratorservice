package com.app.dsgs.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.app.dsgs.model.ContainerData;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceRetrieval {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceRetrieval.class);

    public List<ContainerData> getAllRunningServices() {
        List<ContainerData> containers = null;

        try {
            // Execute 'docker container ls' command
            Process process = Runtime.getRuntime().exec("docker container ls");

            // Read the output of the command
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            containers = parseDockerContainerList(reader);

            // Display container information
            for (ContainerData container : containers) {
                LOG.info("Container ID   : " + container.getId());
                LOG.info("Image          : " + container.getImage());
                LOG.info("Container name : " + container.getContainerName());
                LOG.info("Port           : " + container.getPort());
                LOG.info("-----------------------");
            }

            // Wait for the process to finish
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            LOG.error(e.getMessage(), e);
        }

        return containers;
    }

    private List<ContainerData> parseDockerContainerList(BufferedReader reader) throws IOException {
        List<ContainerData> containers = new ArrayList<>();
        String line;

        // Skip the header line
        reader.readLine();

        // Read each line of the 'docker container ls' output
        while ((line = reader.readLine()) != null) {
            String[] tokens = line.split("\\s+");
            // Assuming the default format of 'docker container ls' command
            if (tokens.length >= 7) {
                ContainerData container = new ContainerData(tokens[0], tokens[1], tokens[tokens.length - 1], tokens[tokens.length - 2]);
                containers.add(container);
            }
        }

        return containers;
    }
}