package com.app.dsgs.controller;

import com.app.dsgs.producer.KafkaProducer;
import com.app.dsgs.service.impl.ServiceRetrieval;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceController {
    @Autowired
    private ServiceRetrieval serviceRetrieval;

    @Autowired
    private KafkaProducer kafkaProducer;
    @GetMapping("/publish/{topic}")
    public ResponseEntity<String> publish(@PathVariable String topic) throws JsonProcessingException {
        //only needed for testing
        kafkaProducer.sendMessage("hello",topic);
        return ResponseEntity.ok("Message sent to kafka topic");
    }

    @GetMapping("/client/all-running-services")
    public ResponseEntity<?> getAllRunningServices() {
        return new ResponseEntity<>(serviceRetrieval.getAllRunningServices(), HttpStatus.OK);
    }

}
