package com.app.dsgs.model;

public class ContainerData {
    private final String id;
    private final String image;
    private final String containerName;
    private final String port;

    public ContainerData(String id, String image, String containerName, String port) {
        this.id = id;
        this.image = image;
        this.containerName = containerName;
        this.port = port;
    }

    public String getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    public String getContainerName() {
        return containerName;
    }

    public String getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "ContainerData{" +
                "id='" + id + '\'' +
                ", image='" + image + '\'' +
                ", containerName='" + containerName + '\'' +
                ", port='" + port + '\'' +
                '}';
    }
}
