package com.app.dsgs.consumer;

import com.app.dsgs.dao.FreeAPIDataRepository;
import com.app.dsgs.model.FreeAPIData;
import com.app.dsgs.service.impl.RunContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaFreeAPIConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaFreeAPIConsumer.class);
    private final FreeAPIDataRepository freeAPIDataRepository;

    public KafkaFreeAPIConsumer(FreeAPIDataRepository freeAPIDataRepository) {
        this.freeAPIDataRepository = freeAPIDataRepository;
    }

    @KafkaListener(
            topics = "${spring.kafka.topic.name}",
            groupId="${spring.kafka.consumer.group-id}"
    )
    public void consume(FreeAPIData eventMessage){
        LOGGER.info(String.format("Event message received -> %s", eventMessage));
        // Create object of WikimediaData to save WikiEventData to database
        FreeAPIData freeAPIData = new FreeAPIData();
        
        freeAPIData.setUrl(eventMessage.getUrl());
        freeAPIData.setApiKey(eventMessage.getApiKey());
        freeAPIData.setName(eventMessage.getName());
        freeAPIData.setType(eventMessage.getType());
        freeAPIData.setDescription(eventMessage.getDescription());

        freeAPIDataRepository.save(freeAPIData);
        RunContainer.run(freeAPIData.getUrl(),"RTDS");
    }
}

