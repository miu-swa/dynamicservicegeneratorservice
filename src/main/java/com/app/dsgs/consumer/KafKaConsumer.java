package com.app.dsgs.consumer;

import com.app.dsgs.service.IDSGSService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.RetryableTopic;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.retrytopic.RetryTopicConfiguration;
import org.springframework.kafka.retrytopic.RetryTopicConfigurationBuilder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class KafKaConsumer {
    @Autowired
    IDSGSService service;
    private static final Logger LOGGER = LoggerFactory.getLogger(KafKaConsumer.class);
    @KafkaListener(id = "rtdi",topicPattern = "RTDI.*", groupId = "rtdi")
    public void consumeRTDI(ConsumerRecord<?,?> message) throws JsonProcessingException {
        System.out.println("rtdi topic parameter:"+message.topic());
        service.createCDS(message);
    }
    @KafkaListener(id = "rtd",topicPattern = "RTD.*", groupId = "rtd")
    public void consumeRTD(ConsumerRecord<?,?> message) throws JsonProcessingException {
        System.out.println("rtd topic parameter:"+message.topic());
        if(!service.getJDRS()){
            service.createJDRS(message);
            service.setJDRS(true);
        }
    }
    @KafkaListener(id = "cd",topicPattern = "CD.*", groupId = "cd")
    public void consumeCD(ConsumerRecord<?,?> message) throws JsonProcessingException {
        System.out.println("cd topic parameter:"+message.topic());
        int id = service.getCDId();
        id++;
        service.setCD(id);
        service.setCDId(id);
        service.createSS(message);
    }
}
