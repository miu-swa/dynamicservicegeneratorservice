package com.app.dsgs.dao;

import com.app.dsgs.model.FreeAPIData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FreeAPIDataRepository extends MongoRepository<FreeAPIData, Long> {
}
